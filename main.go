package main

import (
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	url2 "net/url"
	"os"

	"strings"

	"github.com/gorilla/mux"
)

const (
	LinksTable = `CREATE TABLE links (
		id int NOT NULL AUTO_INCREMENT,
		slug varchar(255),
		long_link varchar(255),
		PRIMARY KEY (id)
	)`

	VisitsTable = `CREATE TABLE visits (
		slug varchar(255) NOT NULL,
		visit_time DATETIME NOT NULL,
		PRIMARY KEY (slug)
	)`
)

var (
	apiPort       = flag.String("http_port", "8080", "HTTP Port")
	dbUser        = os.Getenv("DB_USER")
	dbPass        = os.Getenv("DB_PASSWORD")
	dbConn        = os.Getenv("DB_CONNECTION")
	dbName        = os.Getenv("DB_DATABASE")
	shortLinkHost = "shor.ty"
)

var (
	shortSvc *LinkShortenerService
	countSvc *LinkCounterService
)

func main() {
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s", dbUser, dbPass, dbConn, dbName)
	db, _ := sql.Open("mysql", dsn)
	createTables(db)

	shortSvc = NewLinkShortenerService(db)
	countSvc = NewLinkCounterService(db)

	r := mux.NewRouter()
	r.HandleFunc("/api/link", LinkShortenerHandler).
		Methods("POST")
	r.HandleFunc("/api/counter", LinkCounterHandler).
		Methods("GET")
	r.HandleFunc("/{slug}", LinkRedirectHandler).
		Methods("GET")

	log.Printf("Shorty listening on port %s", *apiPort)
	log.Fatal(http.ListenAndServe(":"+*apiPort, r))
}

func LinkRedirectHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	slug := vars["slug"]
	originalUrl := shortSvc.GetOriginalLink(slug)
	fmt.Println(originalUrl)
	defer countSvc.AddVisit(slug)
	http.Redirect(w, r, originalUrl, http.StatusTemporaryRedirect)
}

type LinkShortenRequest struct {
	OriginalLink string `json:"link"`
}

type LinkShortenResponse struct {
	ShortLink string `json:"short_link"`
}

func LinkShortenerHandler(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, "Server Error", http.StatusInternalServerError)
		return
	}

	var lsr LinkShortenRequest
	err = json.Unmarshal(b, &lsr)
	if err != nil {
		http.Error(w, "Server Error", http.StatusInternalServerError)
		return
	}

	shortLink := shortSvc.GetShortLink(lsr.OriginalLink)
	if shortLink == "" {
		http.Error(w, "Unable to generate shortlink", http.StatusInternalServerError)
		return
	}

	res, err := json.Marshal(LinkShortenResponse{ShortLink: shortLink})
	if err != nil {
		http.Error(w, "Unable to generate shortlink", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}

type VisitResponse struct {
	Visits int `json:"visits"`
}

func LinkCounterHandler(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	link := params.Get("link")
	time := params.Get("time")

	url, err := url2.Parse(link)
	slug := strings.TrimLeft(url.RequestURI(), "/")

	var visits int
	if time == "24h" {
		visits = countSvc.VisitsLastDay(slug)
	} else if time == "7d" {
		visits = countSvc.VisitsLastWeek(slug)
	} else {
		visits = countSvc.Visits(slug)
	}

	res, err := json.Marshal(VisitResponse{Visits: visits})
	if err != nil {
		http.Error(w, "Server Error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(res)
}

// Kind of a hacky way to bootstrap the app for this demo
func createTables(db *sql.DB) {
	_, err := db.Exec(LinksTable)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(VisitsTable)
	if err != nil {
		panic(err)
	}
}
