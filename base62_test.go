package main

import "testing"

func TestBase62Encode(t *testing.T) {
	// Doing the math...
	// 300 % 62 = 52
	// [52]
	// 300 / 62 = 5
	// 5 % 62 = 5
	// [52, 5]
	// [5, 52]
	// 4Q
	id := int64(300)
	encoded := Base62Encode(id)

	expectedResult := "4Q"

	if encoded != expectedResult {
		t.Logf("%s did not equal %s, failure.", encoded, expectedResult)
		t.Fail()
	}
}

func TestBase62Decode(t *testing.T) {
	slug := "4Q"
	decoded := Base62Decode(slug)

	expectedResult := int64(300)

	if decoded != expectedResult {
		t.Logf("%s did not equal %s, failure.", decoded, expectedResult)
		t.Fail()
	}
}
