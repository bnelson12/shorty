package main

import (
	"database/sql"

	"sync"

	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const (
	NewLinkQuery         = `INSERT INTO links(long_link) VALUES (?)`
	UpdateLinkQuery      = `UPDATE links SET slug = ? WHERE id = ?`
	GetOriginalLinkQuery = `SELECT long_link FROM links WHERE id = ?`
	AddVisitQuery        = `INSERT INTO visits(slug, visit_time) VALUES (?, NOW())`

	DefaultLinkPrefix = `https://shorty.co/`
)

type Shortener interface {
	GetShortLink(longLink string) string
	GetOriginalLink(slug string) string
}

type LinkShortenerService struct {
	db    *sql.DB
	cache *sync.Map
}

func NewLinkShortenerService(db *sql.DB) *LinkShortenerService {
	return &LinkShortenerService{db: db, cache: new(sync.Map)}
}

func (svc LinkShortenerService) GetShortLink(longLink string) string {
	// Insert the long link in the database to get the next ID to convert
	stmt, err := svc.db.Prepare(NewLinkQuery)
	if err != nil {
		return ""
	}
	res, err := stmt.Exec(longLink)
	if err != nil {
		return ""
	}
	id, err := res.LastInsertId()

	// Perform the base62 conversion on the ID to acquire the slug
	slug := Base62Encode(id)

	// TODO: Generate and append checksum

	// Update the DB with the new shortlink in a goroutine
	shortLink := svc.addSlug(slug)
	defer svc.saveShortLink(id, shortLink)

	return shortLink
}

func (svc LinkShortenerService) saveShortLink(id int64, shortLink string) {
	stmt, _ := svc.db.Prepare(UpdateLinkQuery)
	stmt.Exec(shortLink, id)
}

func (svc LinkShortenerService) addSlug(slug string) string {
	return fmt.Sprintf("%s%s", DefaultLinkPrefix, slug)
}

func (svc LinkShortenerService) GetOriginalLink(slug string) string {
	var originalLink string
	id := Base62Decode(slug)
	fmt.Println(id)
	err := svc.db.QueryRow(GetOriginalLinkQuery, id).Scan(&originalLink)
	if err != nil {
		return ""
	}
	return originalLink
}
