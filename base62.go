package main

import (
	"bytes"
	"strings"
)

const alpha = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func Base62Encode(id int64) string {
	var idx []int64

	for id > 0 {
		r := id % int64(len(alpha))
		idx = append(idx, r)
		id = id / int64(len(alpha))
	}
	idx = Reverse(idx)

	var buf bytes.Buffer
	for _, v := range idx {
		buf.WriteString(string(alpha[v]))
	}

	return buf.String()
}

func Base62Decode(slug string) int64 {
	idx := int64(0)
	for _, v := range slug {
		idx = idx*int64(len(alpha)) + int64(strings.Index(alpha, string(v)))
	}
	return idx
}

func Reverse(numbers []int64) []int64 {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
	return numbers
}

func Index(vs []string, t string) int {
	for i, v := range vs {
		if v == t {
			return i
		}
	}
	return -1
}
