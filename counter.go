package main

import (
	"database/sql"
)

const (
	GetVisits24HQuery = `SELECT COUNT(*)
	FROM visits WHERE slug = ? AND (visit_time BETWEEN DATE_SUB(NOW(), INTERVAL 24 HOUR) AND NOW())`

	GetVisits7DQuery = `SELECT COUNT(*)
	FROM visits WHERE slug = ? AND (visit_time BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW())`

	GetAllVisitsQuery = `SELECT COUNT(*) FROM visits WHERE slug = ?`
)

type LinkCounter interface {
	VisitsLastDay(slug string) int
	VisitsLastWeek(slug string) int
	Visits(slug string) int
	AddVisit(shortLink string)
}

type LinkCounterService struct {
	db *sql.DB
}

func NewLinkCounterService(db *sql.DB) *LinkCounterService {
	return &LinkCounterService{db}
}

func (lcs LinkCounterService) VisitsLastDay(slug string) int {
	var visits int
	err := lcs.db.QueryRow(GetVisits24HQuery, slug).Scan(&visits)
	if err != nil {
		return -1
	}
	return visits
}

func (lcs LinkCounterService) VisitsLastWeek(slug string) int {
	var visits int
	err := lcs.db.QueryRow(GetVisits7DQuery, slug).Scan(&visits)
	if err != nil {
		return -1
	}
	return visits
}

func (lcs LinkCounterService) Visits(slug string) int {
	var visits int
	err := lcs.db.QueryRow(GetAllVisitsQuery, slug).Scan(&visits)
	if err != nil {
		return -1
	}
	return visits
}

func (lcs LinkCounterService) AddVisit(slug string) error {
	stmt, err := lcs.db.Prepare(AddVisitQuery)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(slug)
	if err != nil {
		return err
	}
	return nil
}
