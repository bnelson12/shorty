# Shorty URL Generator

Application for CloudFlare take home exercise

## Usage
The application can be built and run using docker-compose:

```
$ docker-compose up -d
```

This builds an application container, spins up a mysql container, and starts the API. The application will bootstrap the
DB if the tables do not exist.

To generate a shortlink:
```
$ curl -X POST \
  http://localhost:8080/api/link \
  -H 'content-type: application/json' \
  -d '{
	"link": "http://www.mysuperlonglink.com/please/shorten/me"
}'
```

To redirect to your original link:
```
$ curl -v -H "Host: shor.ty" http://localhost:8080/{yourSlug}
```

To access the visits for a given shortlink:
```
$ curl -X GET \
  'http://localhost:8080/api/counter?link=http%3A%2F%2Fshor.ty%2F2'
```

You may also supply an additional "time" GET parameter of 24h or 7d to retrieve visits for those time periods.


## Design Decisions

My initial approach for this project was to use some sort of hash of the given original URL, but was unhappy with the results. After
some research I found an article about bijective functions and their use in URL shorteners. I proceeded with this implementation using
the base 10 MySQL index and converting it into a base 62 representation for lookup in my alphanumeric dictionary.

I chose to use MySQL for link storage as well as visit "metrics". In an ideal world I would probably set up metric collection similar to how
we are currently doing it at my present job: exporting some counters to a small collector service and rolling them up as time series data in
Hbase.

My original code didn't use gorilla/mux, but I needed a way to differentiate between the API and redirect services so I had to import
this external package to save a bit of time.

The small amount of test coverage I have in this application is around the base62 functionality of the app. If you are interested in running the
tests it's as easy as exec'ing into the Go container and running "go test" in the application root.

A weird thing I had to do was do a bunch of 64 bit integer type  due to the conversion Go MySQL driver's auto increment ID return type.

## TODO's

Unfortunately I ran out of time to complete the remaining task I was looking forward to covering: link enumeration. My idea was to come up with
some sort of hash or checksum function that would add some element of randomness to the end of each shortlink to try to mitigate this risk.

Another big area missing which you may also see that I started fiddling with is caching. I had hoped to at least implement a small in-memory cache
for "hot" links which would be backed by some repository to later be swapped out with Redis or Memcached.

Finally, my application build and bootstrapping methods are kind of jank so that would be a TODO as well as some nice godocs for methods, vars, consts, etc.
